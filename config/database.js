// module.exports = ({ env }) => ({
//   connection: {
//     client: 'mysql',
//     connection: {
//       host: env('DATABASE_HOST', '127.0.0.1'),
//       port: env.int('DATABASE_PORT', 8889),
//       database: env('DATABASE_NAME', 'restaurant-cfa'),
//       user: env('DATABASE_USERNAME', 'root'),
//       password: env('DATABASE_PASSWORD', 'root'),
//       ssl: env.bool('DATABASE_SSL', false),
//     },
//   },
// });


const { parse } = require("pg-connection-string");


module.exports = ({ env }) => {
  const { host, port, database, user, password } = parse(env("DATABASE_URL"));

  return {
    connection: {
      client: 'mysql',
      connection: {
        host,
        port,
        database,
        user,
        password,
        ssl: env("NODE_ENV", "development") === "production" ? { rejectUnauthorized: false } : false,
      },
      debug: false,
    },
  }

  // return env("NODE_ENV", "development") === "production" ? prod : dev
}
